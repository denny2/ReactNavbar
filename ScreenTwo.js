import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Alert
} from 'react-native';

export default class ScreenTwo extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: `${navigation.state.params.screen}`,
    }
  };

  _onPressButton() {
    Alert.alert('You tapped the button!')
  }

    render() {
        const { state, navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
              <Text style={styles.welcome}>
                Welcome To ScreenTwo
              </Text>

              <TouchableHighlight
                    onPress={this._onPressButton}
                    style={styles.button}>
                    <Text style={styles.buttonText}>Button</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      // flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    button: {
      height: 50,
      width: 100,
      alignItems: 'center',
      backgroundColor: 'red',
      marginBottom: 12,
    },
    buttonText: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },

  });