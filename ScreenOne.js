import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} from 'react-native';

export default class ScreenOne extends Component {

    static navigationOptions = {  
        title: "Welcome"
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <TouchableHighlight
                    onPress={() => navigate("ScreenTwo", {screen: "Screen Two"})}
                    style={styles.button}>
                    <Text style={styles.buttonText}>Pressed</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    button: {
        height: 50,
        width: 100,
        alignItems: 'center',
        backgroundColor: 'red',
    },
    buttonText: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    }
  });